import json
from psychopy import visual, core, event
import numpy as np
import time
import ast
from classes import constants
from classes.Tools import Tools
from random import shuffle
import gc
import tempfile
from pyedfread import edf
import os
from threading import Thread
from scipy import ndimage


class Trial:
    trial_info = {}

    # my_config: configparser

    def __init__(self, participant_name, new_id, orig_id, mode, config, generated_trial, eyetracker, version):
        self.participant_name = participant_name;
        self.trial_info['id'] = new_id
        self.trial_info['orig_id'] = orig_id
        self.trial_info['signal_present'] = generated_trial['signal_present']
        self.trial_info['signal_size'] = generated_trial['signal_size']
        self.trial_info['signal_contrast'] = generated_trial['signal_contrast']
        self.trial_info['response'] = None
        self.trial_info['stimulus_on'] = -1
        self.trial_info['stimulus_off'] = -1
        self.trial_info['response_on'] = -1
        self.trial_info['response_off'] = -1
        self.trial_info['response'] = -1
        self.trial_info['marked_locations'] = []
        self.trial_info['locations'] = generated_trial['locations']
        self.trial_info['distractors'] = generated_trial['distractors']
        self.trial_info['version'] = version
        self.signal = generated_trial['signal']

        self.my_config = config
        self.mode = mode
        self.noise = generated_trial['noise']
        if len(self.noise.shape) == 3:
            self.noise_size = (self.noise.shape[2], self.noise.shape[1], self.noise.shape[0])
        else:
            self.noise_size = (self.noise.shape[1], self.noise.shape[0], 1)
        self.eyetracker = eyetracker

        self.eye_tracked = self.my_config.getboolean('experiment', 'eye_tracked')
        self.trial_info['eye_tolerance'] = \
            self.my_config.getfloat('experiment', 'eye_tolerance_dva') / self.my_config.getfloat('experiment',
                                                                                                 'pix2dva')
        if self.mode.find('numbers') == -1:
            if self.trial_info['signal_size'] < 7:
                self.signal_string = 'MICROCALCIFICATION'
                self.s = 0
            else:
                self.signal_string = 'MASS'
                self.s = 1
        else:
            self.signal_string = ''
            self.s = 0
            self.trial_info['generated_number'] = np.random.randint(1000, 9999)

        self.noise_size_display = [-1, -1]
        self.mult_pixel = 1
        self.pos_display = (0, 0)
        self.ratio_img_screen = [1, 1]
        self.fixation_point = [self.noise_size[0] / 2, self.noise_size[1] / 2]
        self.drag_and_drop = False

        self.fixation_spots = []
        if self.mode.find('ff') != -1:
            eccentricities = ast.literal_eval(self.my_config.get('experiment', 'ff_eccentricities_dva'))
            pix2dva = self.my_config.getfloat('experiment', 'pix2dva')
            ff_per_eccentricity = list(map(int, self.my_config.get('experiment', 'ff_per_eccentricity').split(',')))
            for e in eccentricities[self.s]:
                if e == 0:
                    self.fixation_spots.append([e, e])
                else:  # generate circle
                    radius = e / pix2dva
                    for n in range(ff_per_eccentricity[self.s]):
                        p = [radius * np.cos(2 * np.pi * (n / ff_per_eccentricity[self.s])),
                             radius * np.sin(2 * np.pi * (n / ff_per_eccentricity[self.s]))]
                        self.fixation_spots.append(p)

        self.force_mode = 0
        if self.mode.find('scanner') != -1:
            self.scanner_skip_slices = self.my_config.getint('experiment', 'scanner_skip_slices', fallback=1)
            self.scanner_coverage = self.my_config.getfloat('experiment', 'scanner_coverage', fallback=0.8)
            self.force_mode = constants.SCANNER_MODE
        if self.mode.find('driller') != -1:
            self.driller_fps = self.my_config.getfloat('experiment', 'driller_fps', fallback=13)
            self.driller_fixation_grid = []
            config_fixation_grid = ast.literal_eval(self.my_config.get('experiment', 'driller_fixation_grid'))
            for e in config_fixation_grid:
                self.driller_fixation_grid.append(e)
            self.force_mode = constants.DRILLER_MODE

        self.second_chance_probability = self.my_config.getfloat('experiment', 'second_chance_probability', fallback=0)

        self.font_text = 'Courier New'

        self.trial_info['scrolls'] = []
        self.trial_info['pos'] = []
        self.trial_info['eye_movements'] = []
        self.first_chance = None

    def to_json(self):
        t1 = None
        if self.first_chance:
            t1 = json.dumps(self.first_chance, default=lambda o: o.__dict__, sort_keys=False, indent=' ')
        return t1, json.dumps(self.trial_info, default=lambda o: o.__dict__, sort_keys=False, indent=' ')

    def show_init(self, win):

        if self.mode.find('dbt') != -1:
            gray_background = visual.Rect(win, width=2, height=2, fillColor=[0, 0, 0],
                                          fillColorSpace="rgb255", units="norm", lineWidth=0)
            # self.fixation_point = list(win.size / 2)
        else:
            gray_background = visual.Rect(win, width=2, height=2, fillColor=constants.COLOR_BACKGROUND,
                                          fillColorSpace="rgb255", units="norm", lineWidth=0)

        event.clearEvents()
        my_mouse = event.Mouse()

        attention_point = None
        if self.mode.find('ff') != -1:
            if self.trial_info['signal_present']:
                attention_point = [self.trial_info['locations'][0], self.trial_info['locations'][1]]
            else:
                np.random.seed(self.trial_info['id'])
                attention_point = [100 + np.random.randint(self.noise_size[0] - 100),
                                   100 + np.random.randint(self.noise_size[1] - 100)]
            shuffle(self.fixation_spots)
            self.fixation_point = [-1, -1]
            c = 0
            while not (0 < self.fixation_point[0] < self.noise_size[0] and
                       0 < self.fixation_point[1] < self.noise_size[1]):
                self.fixation_point = np.add(attention_point, self.fixation_spots[c]).tolist()
                c += 1

        screen_fixation_point = win.size / 2 + np.array(self.fixation_point) - np.divide(self.noise_size[0:2], 2)

        self.trial_info['fixation_point'] = self.fixation_point

        cheating = False
        finished = False
        check_fixation = False
        check_start = -1
        check_length = 1
        sample = [0, 0]
        tracker_event = 0

        if self.eyetracker:
            self.eyetracker.startEyeTracking(1, False, win.size[0], win.size[1])
            # self.eyetracker.sendMessage("!V TRIAL_VAR_DATA 0")

        # while True:
        #     tracker_event = self.eyetracker.getNextData()
        #     if tracker_event == 5:
        #         print(tracker_event)
        #     keys = Tools.check_force_quit()
        #     if keys:
        #         if 'return' in keys:
        #             break
        #     visual.TextStim(win, "SACCADEA", colorSpace='rgb255', height=constants.RATING_SIDE / 4, units="pix",
        #                     color=constants.COLOR_WHITE, pos=(0, 0)).draw()
        #     win.flip()

        if self.mode.find('ff') == -1 and self.mode.find('dbt') == -1:
            if self.noise_size[2] > 2:
                signal = self.signal['signal'][0][self.s][:, :, np.int(self.signal['signal'][0][self.s].shape[2] / 2)]
            else:
                signal = self.signal['signal'][0][self.s]
            signal = np.maximum(0, np.multiply(
                np.subtract(np.divide(np.array(signal * 255, dtype=float), 255), 0.5), 2))
        if self.force_mode == constants.DRILLER_MODE:
            while self.post_load_slice < self.noise_size[2]:
                if self.post_load_slice < self.noise_size[2] and self.post_load_slice not in self.stimulus:
                    self.stimulus[self.post_load_slice] = visual.ImageStim(win, self.noise[self.post_load_slice, :, :],
                                                                           size=self.noise_size_display,
                                                                           pos=self.pos_display, flipVert=True)
                self.post_load_slice += 1

        while not finished:
            gray_background.draw()
            mouse_pos = my_mouse.getPos()

            if self.post_load_slice < self.noise_size[2] and self.post_load_slice not in self.stimulus:
                self.stimulus[self.post_load_slice] = visual.ImageStim(win, self.noise[self.post_load_slice, :, :],
                                                                       size=self.noise_size_display,
                                                                       pos=self.pos_display, flipVert=True)
            self.post_load_slice += 1

            if attention_point != self.fixation_point:
                # Tools.draw_cross(win, [(self.pos_display[0] - self.noise_size_display[0] / 2 +
                #                         self.fixation_point[0] * self.ratio_img_screen[1]),
                #                        (self.pos_display[1] + self.noise_size_display[1] / 2 -
                #                         self.fixation_point[1] * self.ratio_img_screen[1])])
                # Tools.draw_cross(win, [(self.fixation_point[0] - self.pos_display[0]),
                #                        (self.fixation_point[1] - self.pos_display[1] - self.noise_size[1]/2)])
                if self.mode.find('ff') == -1 and self.mode.find('dbt') == -1:
                    # signal = np.array(signal['signal'][0][0]*255, dtype=float)
                    visual.ImageStim(win, signal, units="pix", pos=(0, 0), size=self.noise_size_display).draw()
                else:
                    Tools.draw_cross(win, [screen_fixation_point[0] - win.size[0] / 2,
                                           win.size[1] / 2 - screen_fixation_point[1]])
            visual.TextStim(win, self.signal_string, colorSpace='rgb255', height=constants.RATING_SIDE / 4, units="pix",
                            color=constants.COLOR_WHITE, pos=(0, 50)).draw()

            if attention_point:
                Tools.draw_cross(win, [(self.pos_display[0] - self.noise_size_display[0] / 2 +
                                        attention_point[0] * self.ratio_img_screen[1]),
                                       (self.pos_display[1] + self.noise_size_display[1] / 2 -
                                        attention_point[1] * self.ratio_img_screen[1])],
                                 big=True)

            if self.eyetracker:
                sample = self.eyetracker.getNewestSample(self.eye_tracked, mouse_pos)
                tracker_event = self.eyetracker.getNextData()
                # print(tracker_event, sample)

            if cheating and self.eyetracker:
                # sample = self.eyetracker.getNewestSample(self.eye_tracked, event.Mouse().getPos())

                visual.Circle(win, radius=5, lineWidth=1, lineColorSpace="rgb255", units="pix",
                              lineColor=constants.COLOR_WHITE,
                              pos=(-win.size[0] / 2 + sample[0], win.size[1] / 2 - sample[1])).draw()

                visual.TextStim(win, 'C', colorSpace='rgb255', height=constants.RATING_SIDE / 4,
                                units="pix", color=constants.COLOR_WHITE,
                                pos=(-win.size[0] / 2 + 50, win.size[1] / 2 - 50)).draw()

                # for spot in self.fixation_spots:
                #     visual.Circle(win, radius=3, lineWidth=1, lineColorSpace="rgb255", units="pix",
                #                   lineColor=constants.COLOR_WHITE,
                #                   pos=(fixation_point[0] + spot[0], fixation_point[1] + spot[1])).draw()

            win.flip()

            if check_fixation:
                if tracker_event == constants.STARTSACC or \
                        np.sqrt(np.sum(np.square(np.subtract(sample, screen_fixation_point)))) > \
                        self.trial_info['eye_tolerance']:
                    gray_background.draw()
                    visual.TextStim(win, 'BROKEN FIXATION', colorSpace='rgb255', height=constants.RATING_SIDE / 4,
                                    units="pix", color=constants.COLOR_WHITE, pos=(0, 0)).draw()
                    win.flip()
                    core.wait(1)
                    check_fixation = False
                elif time.time() - check_start > check_length:
                    finished = True

            keys = Tools.check_force_quit(self.eyetracker)
            if keys:
                if 'space' in keys:
                    # bound = dict(left=860, top=440, right=1060, bottom=640)
                    # self.eyetracker.gc(bound=bound, t_min=2000, t_max=10000)
                    if self.eyetracker:
                        check_fixation = True
                        check_length = np.random.uniform(0.75, 1.25)
                        check_start = time.time()
                    else:
                        finished = True
                elif self.eyetracker and 'c' in keys:
                    self.eyetracker.stopEyeTracking()
                    self.eyetracker.startEyeTracking(1, True, win.size[0], win.size[1])
                elif 't' in keys and 'lalt' in keys:
                    cheating = not cheating
                elif 'r' in keys:
                    return False

        # if self.eyetracker:
        #     self.eyetracker.stopEyeTracking()

        return True

    def show_trial(self, win):
        # stimulus = visual.ImageStim(win, noise[:, :, 0], units="pix", size=noise_size)

        if self.mode.find('dbt') != -1:
            gray_background = visual.Rect(win, width=2, height=2, fillColor=[0, 0, 0],
                                          fillColorSpace="rgb255", units="norm", lineWidth=0)
        else:
            gray_background = visual.Rect(win, width=2, height=2, fillColor=constants.COLOR_BACKGROUND,
                                          fillColorSpace="rgb255", units="norm", lineWidth=0)

        current_slice = 0

        double_click = self.my_config.getboolean('experiment', 'double_click')
        double_click_time = 0.15  # seconds
        mouse_down = False
        mouse_release = 0
        new_sample = [0, 0]

        finished = False
        cheating = False
        cheating_coverage = False
        large_scroll_type = self.my_config.getint('experiment', 'large_scroll_style', fallback=0)
        large_drag_speed_multiplier = self.my_config.getfloat('experiment', 'large_drag_speed_multiplier', fallback=1)
        event.clearEvents()

        self.ratio_img_screen = np.divide([win.size[0], win.size[1]], [self.noise_size[0], self.noise_size[1]])
        self.noise_size_display = [np.minimum(self.noise_size[0], self.noise_size[0] * self.ratio_img_screen[1]),
                                   np.minimum(self.noise_size[1], win.size[1])]
        rescaled_image = False

        if self.mode.find('dbt') != -1 and (
                self.noise_size_display[0] != self.noise_size[0] or self.noise_size_display[1] != self.noise_size[1]):
            self.pos_display = (win.size[0] / 2 - self.noise_size_display[0] / 2, 0)
            rescaled_image = True
            self.mult_pixel = 10
        elif self.noise_size[0] > win.size[0] or self.noise_size[1] > win.size[1]:
            self.drag_and_drop = True
            # self.pos_display = [0, 0] # centered
            self.pos_display = [+self.noise_size[0] / 2 - win.size[0] / 2,
                                -self.noise_size[1] / 2 + win.size[1] / 2]  # top left
            self.trial_info['pos'].append([self.pos_display[0], self.pos_display[1], time.time()])
            self.ratio_img_screen = [1, 1]
            prev_mouse = [-1, -1]
            self.noise_size_display = self.noise_size[0:2]
        else:
            self.pos_display = (0, 0)
            self.ratio_img_screen = [1, 1]

        gray_background.draw()
        visual.TextStim(win, 'Generating trial ' + str(self.trial_info['orig_id']),
                        height=constants.RATING_SIDE / 3).draw()
        win.flip()

        self.stimulus = {}

        if self.noise_size[2] > 1:
            self.stimulus[0] = visual.ImageStim(win, self.noise[0, :, :], size=self.noise_size_display,
                                                pos=self.pos_display, flipVert=True)
        else:
            self.stimulus[0] = visual.ImageStim(win, self.noise, size=self.noise_size_display, pos=self.pos_display)

        # noise_slices = []
        # for s in range(0, self.noise_size[2]):
        #     noise_slices.append(self.noise[:, :, s])

        self.post_load_slice = 0

        while not self.show_init(win):
            pass

        screen_fixation_point = win.size / 2 + np.array(self.fixation_point) - [np.divide(self.noise_size[0:2], 2)]
        display_time = -1
        tracker_event = 0
        if self.mode.find('ff') != -1:
            display_time = self.my_config.getfloat('experiment', 'ff_display_time')

        my_mouse = event.Mouse()

        prev_sample = (-1, -1)

        gc.disable()

        # stimulus = visual.ImageStim(win, noise_slices[current_slice], size=self.noise_size[0:2])

        slice_text = visual.TextStim(win, 'Slice (' + str(current_slice + 1) + '/' + str(self.noise_size[2]) + ')',
                                     colorSpace='rgb255', height=constants.RATING_SIDE / 3, units="pix",
                                     color=constants.COLOR_WHITE, pos=(0, np.minimum(win.size[1] / 2 - 50,
                                                                                     self.noise_size[1] / 2 + 50)))

        signal_text = visual.TextStim(win, self.signal_string, colorSpace='rgb255', height=constants.RATING_SIDE / 3,
                                      units="pix", color=constants.COLOR_WHITE,
                                      pos=(0, np.minimum(win.size[1] / 2 - 15, self.noise_size[1] / 2 + 85)))

        ufov = np.round(self.my_config.getfloat('experiment', 'ufov_radius', fallback=2.5) /
                        self.my_config.getfloat('experiment', 'pix2dva', fallback=0.022))

        y, x = np.ogrid[-ufov: ufov + 1, -ufov: ufov + 1]
        maskO = x ** 2 + y ** 2 <= ufov ** 2
        pad_array = (np.asarray([[self.noise_size[1] - maskO.shape[1] + 1, self.noise_size[1] - maskO.shape[1]],
                                 [self.noise_size[0] - maskO.shape[0] + 1, self.noise_size[0] - maskO.shape[0]]]
                                ) // 2).astype(int).tolist()
        mask = np.pad(maskO, pad_array, 'constant', constant_values=0)

        if self.noise_size[2] > 1:
            image_mask = self.noise[0, :, :] > -0.7
            self.coverage = np.zeros([self.noise_size[1], self.noise_size[0], self.noise_size[2]], dtype=bool)
        else:
            image_mask = self.noise > -0.7
            self.coverage = np.zeros([self.noise_size[1], self.noise_size[0]], dtype=bool)

        if self.force_mode == constants.SCANNER_MODE:
            if self.trial_info['signal_present']:
                current_slice = self.trial_info['locations'][2] % self.scanner_skip_slices
            else:
                current_slice = np.random.randint(0, self.scanner_skip_slices)

        quadrant = 0

        if self.eyetracker and not self.eyetracker.dummy:
            self.eyetracker.startEyeTracking(2, False, win.size[0], win.size[1])
            self.eyetracker.sendMessage("TRIAL START")
            # self.eyetracker.sendMessage("!V TRIAL_VAR_DATA 0")
            self.eyetracker.resetData()
        # print("trial starts now")

        self.trial_info['stimulus_on'] = time.time()

        fps_ini = time.time()
        fps_lock = time.time()
        while not finished:
            gray_background.draw()

            if self.noise_size[2] > 1:
                if current_slice not in self.stimulus:
                    self.stimulus[current_slice] = visual.ImageStim(win, self.noise[current_slice, :, :],
                                                                    size=self.noise_size_display,
                                                                    pos=self.pos_display, flipVert=True)

            if self.post_load_slice < self.noise_size[2] and self.post_load_slice not in self.stimulus:
                self.stimulus[self.post_load_slice] = visual.ImageStim(win, self.noise[self.post_load_slice, :, :],
                                                                       size=self.noise_size_display,
                                                                       pos=self.pos_display, flipVert=True)
            self.post_load_slice += 1

            self.stimulus[current_slice].draw()

            if self.drag_and_drop:  # show scroll bar
                if large_scroll_type == 1:
                    self.draw_minimap(win, scroll_bars=True, minimap=False)
                elif large_scroll_type == 2:
                    self.draw_minimap(win, scroll_bars=False, minimap=True)
                elif large_scroll_type == 3:
                    self.draw_minimap(win, scroll_bars=True, minimap=True)

            if self.noise_size[2] > 1:
                slice_text.text = 'Slice (' + str(current_slice + 1) + '/' + str(self.noise_size[2]) + ')'
                slice_text.draw()
            signal_text.draw()

            wheel = my_mouse.getWheelRel()
            mouse_buttons = my_mouse.getPressed()
            mouse_pos = my_mouse.getPos()
            # prev_mouse = mouse_pos

            if self.mode.find('numbers') != -1:
                pos = ((self.pos_display[0] - self.noise_size_display[0] / 2 +
                        self.trial_info['locations'][0] * self.mult_pixel * self.ratio_img_screen[1]),
                       (self.pos_display[1] + self.noise_size_display[1] / 2 -
                        self.trial_info['locations'][1] * self.mult_pixel * self.ratio_img_screen[1]))
                visual.TextStim(win, str(self.trial_info['generated_number']), colorSpace='rgb255',
                                height=constants.RATING_SIDE / 3, units="pix", color=constants.COLOR_WHITE,
                                pos=pos, font=self.font_text, bold=True).draw()
                visual.TextStim(win, str(self.trial_info['generated_number']), colorSpace='rgb255',
                                height=constants.RATING_SIDE / 3, units="pix", color=constants.COLOR_WHITE,
                                pos=(pos[0] + 5, pos[1]), font=self.font_text, bold=True).draw()

            direction = np.nan
            if wheel[1] != 0:
                direction = -1
                if wheel[1] < 0:
                    direction = 1
            elif self.force_mode == constants.SCANNER_MODE and \
                    np.sum(self.coverage[:, :, current_slice]) / np.sum(image_mask) > self.scanner_coverage:
                direction = self.scanner_skip_slices
                if current_slice >= self.noise_size[2] - 1:
                    finished = True
            elif self.force_mode == constants.DRILLER_MODE and self.eyetracker and \
                    time.time() - fps_lock > 1 / self.driller_fps:
                if np.sqrt(np.sum(np.square(
                        np.subtract((new_sample - (win.size - np.array(self.noise_size_display)) / 2 - np.array(
                            self.pos_display)) / self.ratio_img_screen[1],
                                    self.driller_fixation_grid[quadrant])))) < self.trial_info['eye_tolerance'] * 2:
                    direction = 2 * (1 - quadrant % 2) - 1
                    fps_lock = time.time()
                    if current_slice + direction >= self.noise_size[2] or current_slice + direction <= 0:
                        quadrant += 1
                    if quadrant >= len(self.driller_fixation_grid):
                        finished = True
                else:
                    pos = ((self.pos_display[0] - self.noise_size_display[0] / 2 +
                            self.driller_fixation_grid[quadrant][0] * self.ratio_img_screen[1]),
                           (self.pos_display[1] + self.noise_size_display[1] / 2 -
                            self.driller_fixation_grid[quadrant][1] * self.ratio_img_screen[1]))
                    visual.Circle(win, radius=50, lineWidth=4, lineColorSpace="rgb255", units="pix",
                                  lineColor=constants.COLOR_WHITE, pos=pos).draw()

            if any(mouse_buttons) and not mouse_down:
                mouse_down = True
                # print(time.time() - mouse_release)

                if double_click and time.time() - mouse_release < double_click_time and \
                        my_mouse.isPressedIn(self.stimulus[current_slice]):
                    if rescaled_image:
                        new_mark = [
                            (mouse_pos[0] + win.size[0] / 2 - self.pos_display[0] * 2) / self.ratio_img_screen[
                                1] / self.mult_pixel,
                            (-mouse_pos[1] + win.size[1] / 2) / self.ratio_img_screen[1] / self.mult_pixel,
                            current_slice + 1, time.time()]
                    else:
                        new_mark = [mouse_pos[0] + self.noise_size[0] / 2 - self.pos_display[0],
                                    -mouse_pos[1] + self.noise_size[1] / 2 + self.pos_display[1],
                                    current_slice + 1, time.time()]
                    # print(new_mark)

                    delete = -1
                    for i in range(0, len(self.trial_info['marked_locations'])):
                        dist = np.sqrt(
                            np.sum(np.subtract(self.trial_info['marked_locations'][i][0:3], new_mark[0:3]) ** 2))
                        if dist * self.mult_pixel < 30:
                            delete = i
                            break
                    if delete == -1:
                        self.trial_info['marked_locations'].append(new_mark)
                    else:
                        del self.trial_info['marked_locations'][delete]
            elif not any(mouse_buttons) and mouse_down:
                mouse_release = time.time()
                mouse_down = False
                prev_mouse = [-1, -1]
            elif self.drag_and_drop and any(mouse_buttons) and mouse_down:
                if prev_mouse[0] == -1 and prev_mouse[1] == -1:
                    prev_mouse[:] = mouse_pos[:]
                else:
                    stimulus_pos = self.stimulus[current_slice].pos
                    if large_drag_speed_multiplier == 0:
                        self.pos_display = (stimulus_pos - (prev_mouse - mouse_pos))
                    else:
                        self.pos_display = (stimulus_pos - (prev_mouse - mouse_pos) * large_drag_speed_multiplier
                                            * self.noise_size[0:2] / np.array([win.size[0], win.size[1]]))
                    self.stimulus[current_slice].setPos((self.pos_display[0], self.pos_display[1]))
                    prev_mouse[:] = mouse_pos[:]
                    self.trial_info['pos'].append([self.pos_display[0], self.pos_display[1], time.time()])

            if double_click:
                for answer in self.trial_info['marked_locations']:
                    if np.abs(answer[2] - current_slice + 1) < 5:
                        pos = ((self.pos_display[0] - self.noise_size_display[0] / 2 +
                                answer[0] * self.mult_pixel * self.ratio_img_screen[1]),
                               (self.pos_display[1] + self.noise_size_display[1] / 2 -
                                answer[1] * self.mult_pixel * self.ratio_img_screen[1]))
                        visual.Circle(win, radius=50, lineWidth=4, lineColorSpace="rgb255", units="pix",
                                      lineColor=constants.COLOR_WHITE,
                                      pos=(pos[0], pos[1])).draw()
            fps_end = time.time() - fps_ini
            fps_ini = time.time()
            if cheating:
                visual.TextStim(win, ("%.1f" % (1 / np.max([.00000000001, fps_end]))), height=15, alignText='left',
                                pos=(-win.size[0] / 2 + 30, win.size[1] / 2 - 30)).draw()
                if cheating_coverage:
                    if self.noise_size[2] > 1:
                        visual.ImageStim(win,
                                         self.coverage[:, :, current_slice].astype(int) * 2 - 1 + image_mask.astype(
                                             int),
                                         size=self.noise_size_display, pos=self.pos_display, flipVert=True).draw()
                    else:
                        visual.ImageStim(win, self.coverage.astype(int) * 2 - 1 + image_mask.astype(int),
                                         size=self.noise_size_display, pos=self.pos_display, flipVert=True).draw()
                visual.TextStim(win, 'Location: ' + str(self.trial_info['locations'][0]) + ', ' +
                                str(self.trial_info['locations'][1]) + ', ' + str(self.trial_info['locations'][2]),
                                colorSpace='rgb255', height=constants.RATING_SIDE / 3, units="pix",
                                color=constants.COLOR_WHITE, pos=(0, -win.size[1] / 2 + 100)).draw()

                if self.trial_info['signal_present'] and \
                        (np.abs(current_slice + 1 - self.trial_info['locations'][2]) < 5 or self.noise_size[2] == 1):
                    pos = ((self.pos_display[0] - self.noise_size_display[0] / 2 +
                            self.trial_info['locations'][0] * self.mult_pixel * self.ratio_img_screen[1]),
                           (self.pos_display[1] + self.noise_size_display[1] / 2 -
                            self.trial_info['locations'][1] * self.mult_pixel * self.ratio_img_screen[1]))
                    visual.Circle(win, radius=50, lineWidth=4, lineColorSpace="rgb255", units="pix",
                                  lineColor=constants.COLOR_WHITE, pos=pos).draw()

            if self.noise_size[2] > 1:
                new_direction = self.draw_scrollbar(win, current_slice, mouse_pos, mouse_buttons, rescaled_image)
                if not np.isnan(new_direction):
                    direction = new_direction

            win.flip()

            # win.getMovieFrame(buffer='front')

            keys = Tools.check_force_quit(self.eyetracker)
            if keys:
                if 'space' in keys:
                    finished = True
                    self.trial_info['stimulus_off'] = time.time()
                elif 'c' in keys and 'lalt' in keys:
                    cheating = not cheating
                elif cheating and 'v' in keys and 'lalt' in keys:
                    cheating_coverage = not cheating_coverage

            if self.eyetracker:
                # tracker_float = self.eyetracker.getFloatData()
                new_sample = self.eyetracker.getNewestSample(self.eye_tracked, mouse_pos)
                tracker_event = self.eyetracker.getNextData()  # getFloatData()
                # print(tracker_event, new_sample)
                # if not (new_sample[0] == prev_sample[0] and new_sample[1] == prev_sample[1]):
                prev_sample = (new_sample[0], new_sample[1])
                self.trial_info['eye_movements'].append((new_sample[0], new_sample[1], time.time()))
                # if tracker_event == constants.STARTSACC:
                #     self.trial_info['start_saccades'].append((new_sample[0], new_sample[1], time.time()))
                # elif tracker_event == constants.ENDSACC:
                #     self.trial_info['end_saccades'].append((new_sample[0], new_sample[1], time.time()))
                if rescaled_image:
                    shift_coverage = [
                        np.int(new_sample[1] / self.ratio_img_screen[1] - self.coverage.shape[0] // 2),
                        np.int(
                            (new_sample[0] - self.pos_display[0] * 2) / self.ratio_img_screen[1] - self.coverage.shape[
                                1] // 2)]
                else:
                    shift_coverage = [np.int(new_sample[1] - win.size[1] / 2),
                                      np.int(new_sample[0] - win.size[0] / 2)]
                if self.noise_size[2] > 1:
                    self.coverage[:, :, current_slice] = \
                        np.logical_and(image_mask, np.logical_or(self.coverage[:, :, current_slice],
                                                                 Tools.shift_matrix(mask, shift_coverage, 0)))
                else:
                    self.coverage = np.logical_and(image_mask,
                                                   np.logical_or(self.coverage,
                                                                 Tools.shift_matrix(mask, shift_coverage, 0)))
            else:
                new_sample = [-1, -1]

            if not np.isnan(direction):
                direction = int(direction)
                current_slice = min(self.noise_size[2] - 1, max(0, current_slice + direction))
                self.trial_info['scrolls'].append((direction, time.time()))
                # if self.noise_size[2] == 1:
                #     self.coverage = np.zeros_like(self.coverage)

            if display_time > 0:
                if tracker_event == constants.STARTSACC or \
                        np.sqrt(np.sum(np.square(np.subtract(new_sample, screen_fixation_point)))) > \
                        self.trial_info['eye_tolerance']:
                    gray_background.draw()
                    visual.TextStim(win, 'BROKEN FIXATION', colorSpace='rgb255', height=constants.RATING_SIDE / 4,
                                    units="pix", color=constants.COLOR_WHITE, pos=(0, 0)).draw()
                    win.flip()
                    core.wait(1)
                    self.trial_info['response'] = -1
                    self.trial_info['stimulus_off'] = time.time()
                    return False
                if time.time() - self.trial_info['stimulus_on'] > display_time:
                    finished = True
                    self.trial_info['stimulus_off'] = time.time()

        # print("trial stops now")

        if self.eyetracker and not self.eyetracker.dummy:
            self.eyetracker.sendMessage("TRIAL END")
            self.eyetracker.stopEyeTracking()
        gc.enable()
        # win.saveMovieFrames(fileName='recording.mp4')

        return True
        # tracker_job.shutdown_flag.set()
        # tracker_job.join()
        # self.trial_info['eye_movements'] = tracker_job.eye_movements

    def get_response_numbers(self, win):
        self.trial_info['response_on'] = time.time()

        input_number = ''

        finished = False
        while not finished:
            visual.Line(win, start=(-100, 45), end=(100, 45), units="pix", lineWidth=2,
                        lineColorSpace="rgb255", lineColor=constants.COLOR_WHITE).draw()
            visual.TextStim(win, input_number, color=constants.COLOR_WHITE, height=constants.RATING_SIDE / 3,
                            units="pix", colorSpace="rgb255", pos=(0, 45), alignText='center',
                            anchorVert='bottom', font=self.font_text, bold=True).draw()
            win.flip()

            keys = Tools.check_force_quit(self.eyetracker)
            if keys:
                if keys[0] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'num_0', 'num_1', 'num_2',
                               'num_3', 'num_4', 'num_5', 'num_6', 'num_7', 'num_8', 'num_9']:
                    input_number += keys[0][-1]
                elif 'return' in keys or 'tab' in keys and input_number != []:
                    finished = True
                elif 'backspace' in keys:
                    input_number = input_number[:-1]

        self.trial_info['response'] = int(input_number)
        self.trial_info['marked_locations'] = [-1, -1, -1]
        self.trial_info['response_off'] = time.time()

    def retrieve_edf_file(self):
        self.eyetracker.retrieveDataFile(tempfile.gettempdir() + '/trial.edf')

        samples, events, messages = edf.pread(tempfile.gettempdir() + '/trial.edf')
        os.remove(tempfile.gettempdir() + '/trial.edf')

        start_saccade = None
        saccades = []
        all_saccades = []
        all_fixations = []
        for i in range(len(events)):
            if events.type[i] == "fixation" and start_saccade:
                saccades.append([start_saccade, [events.gstx[i], events.gsty[i],
                                                 self.trial_info['stimulus_on'] + (float(events.end[i]) / 1000)]])
                all_fixations.append([events.gavx[i], events.gavy[i],
                                      self.trial_info['stimulus_on'] + (float(events.start[i]) / 1000),
                                      self.trial_info['stimulus_on'] + (float(events.end[i]) / 1000)])
                start_saccade = None

            if events.type[i] == "saccade":
                start_saccade = [events.gstx[i], events.gsty[i],
                                 self.trial_info['stimulus_on'] + (float(events.end[i]) / 1000)]
                all_saccades.append([[events.gstx[i], events.gsty[i],
                                      self.trial_info['stimulus_on'] + (float(events.start[i]) / 1000)],
                                     [events.genx[i], events.geny[i],
                                      self.trial_info['stimulus_on'] + (float(events.end[i]) / 1000)]])

        if self.my_config.getint('experiment', 'edf_eyemovements', fallback=False):
            eye_movements = []
            for i in range(len(samples)):
                if self.eye_tracked == 0:  # right eye
                    eye_movements.append([samples.gx_right[i], samples.gy_right[i],
                                          self.trial_info['stimulus_on'] + (samples.time[i] / 1000)])
                else:  # left eye
                    eye_movements.append([samples.gx_left[i], samples.gy_left[i],
                                          self.trial_info['stimulus_on'] + (samples.time[i] / 1000)])
            # print(saccades)
            self.trial_info['eye_movements'] = eye_movements

        self.trial_info['saccades'] = saccades
        self.trial_info['all_saccades'] = all_saccades
        self.trial_info['all_fixations'] = all_fixations

    def get_response_percentage(self, win, ask_percentage):
        my_mouse = event.Mouse()

        width_percentage = 0.6
        bar_height = 30
        bar_width = win.size[0] * width_percentage
        current_percentage = 0.5

        finished = False

        while not finished:
            visual.TextStim(win, ask_percentage, colorSpace="rgb255",
                            height=constants.RATING_SIDE / 3, wrapWidth=None, color=constants.COLOR_WHITE,
                            pos=(0, 200)).draw()

            # visual.Line(win, start=(-bar_width / 2, 0),
            #             end=(+bar_width / 2, 0), units="pix", lineWidth=bar_height,
            #             lineColorSpace="rgb255", lineColor=constants.COLOR_RED).draw()
            visual.Rect(win, width=bar_width, height=bar_height * 3, lineWidth=0,
                        units="pix", fillColorSpace="rgb255", fillColor=[0.7 * x for x in constants.COLOR_RED],
                        pos=(0, 0)).draw()

            # visual.Line(win, start=(-bar_width / 2 + current_percentage * bar_width, bar_height * 2),
            #             end=(-bar_width / 2 + current_percentage * bar_width, -bar_height * 2), units="pix",
            #             lineWidth=bar_height * 3, lineColorSpace="rgb255", lineColor=constants.COLOR_RED).draw()

            visual.Rect(win, width=current_percentage * bar_width, height=bar_height * 3, lineWidth=0,
                        units="pix", fillColorSpace="rgb255", fillColor=constants.COLOR_RED,
                        pos=(-bar_width / 2 + (current_percentage * bar_width) / 2, 0)).draw()

            visual.TextStim(win, str(int(current_percentage * 100)) + "%", colorSpace="rgb255",
                            height=constants.RATING_SIDE / 3, wrapWidth=None, color=constants.COLOR_WHITE,
                            pos=(-bar_width / 2 + current_percentage * bar_width, bar_height * 3)).draw()

            visual.TextStim(win, "Press SPACEBAR to continue", colorSpace='rgb255',
                            height=constants.RATING_SIDE / 3, wrapWidth=None, color=constants.COLOR_WHITE,
                            pos=(0, np.maximum(-win.size[1] / 2 + 50, -self.noise_size[1] / 2 - 50))).draw()

            keys = Tools.check_force_quit(self.eyetracker)
            if keys:
                if 'space' in keys:
                    finished = True

            mouse_buttons = my_mouse.getPressed()

            if any(mouse_buttons):
                mouse_pos = my_mouse.getPos()
                current_percentage = max(0, min(1, (mouse_pos[0] + bar_width / 2) / bar_width))
                # print(mouse_pos)

            win.flip()
        return current_percentage

    def get_response(self, win):
        th = None
        if self.eyetracker and not self.eyetracker.dummy:
            th = Thread(target=self.retrieve_edf_file)
        # visual.TextStim(win, 'Please wait...', color=constants.COLOR_WHITE, units="pix",
        #                 colorSpace="rgb255", alignText='center', pos=(0, 0), height=constants.RATING_SIDE / 3).draw()
        win.flip()

        if self.mode.find('numbers') != -1:
            return self.get_response_numbers(win)

        buttons = []
        num_ratings = self.my_config.getint('experiment', 'confidence_ratings')
        for p in range(0, num_ratings):
            buttons.append((p * (constants.RATING_SIDE + constants.RATING_SEPARATION) -
                            (num_ratings / 2 * constants.RATING_SIDE + ((num_ratings / 2) - 1) *
                             constants.RATING_SEPARATION), -100))

        rect = []
        text = []
        for p in range(0, num_ratings):
            rect.append(visual.Rect(win, width=constants.RATING_SIDE, height=constants.RATING_SIDE, units="pix",
                                    fillColorSpace='rgb255', fillColor=[0, 0, 0], lineWidth=4, lineColorSpace="rgb255",
                                    lineColor=constants.RATING_GREEN,
                                    pos=buttons[p]))
            text.append(visual.TextStim(win, str(p + 1), color=constants.COLOR_WHITE, units="pix",
                                        colorSpace="rgb255", pos=buttons[p], height=constants.RATING_SIDE / 2))

        text.append(visual.TextStim(win, 'ABSENT', color=constants.COLOR_WHITE, units="pix",
                                    colorSpace="rgb255", alignText='center',
                                    pos=((rect[int(num_ratings / 2 - 1)].pos[0] + rect[0].pos[0]) / 2, -15),
                                    height=constants.RATING_SIDE / 3))
        text.append(visual.TextStim(win, 'PRESENT', color=constants.COLOR_WHITE, units="pix",
                                    colorSpace="rgb255",
                                    pos=((rect[-1].pos[0] + rect[int(num_ratings / 2)].pos[0]) / 2, -15),
                                    height=constants.RATING_SIDE / 3))

        finished = False

        my_mouse = event.Mouse()
        event.clearEvents()
        my_mouse.clickReset()

        np.random.seed(self.trial_info['id'])
        second_chance = self.second_chance_probability > np.random.random()

        sel = -1
        self.trial_info['response_on'] = time.time()
        while not finished:
            # gray_background.draw()
            mouse_pos = my_mouse.getPos()
            buttons = my_mouse.getPressed()
            if buttons[0]:
                for p in range(0, num_ratings):
                    if rect[p].contains(mouse_pos):
                        sel = p + 1

            for p in range(0, num_ratings):
                if p < num_ratings / 2:
                    rect[p].lineColor = constants.RATING_RED
                else:
                    rect[p].lineColor = constants.RATING_GREEN

                if rect[p].contains(mouse_pos):
                    rect[p].fillColor = rect[p].lineColor * 0.6
                    if num_ratings in constants.CONFIDENCE:
                        visual.TextStim(win, constants.CONFIDENCE[num_ratings][p], color=constants.COLOR_WHITE,
                                        units="pix",
                                        colorSpace="rgb255", pos=(rect[p].pos[0], rect[p].pos[1] - 70),
                                        alignText='center', height=constants.RATING_SIDE / 4).draw()
                        visual.TextStim(win, "CONFIDENCE", color=constants.COLOR_WHITE, units="pix",
                                        colorSpace="rgb255", pos=(rect[p].pos[0], rect[p].pos[1] - 100),
                                        alignText='center', height=constants.RATING_SIDE / 4).draw()
                else:
                    rect[p].fillColor = [0, 0, 0]
                rect[p].draw()

            if not sel == -1:
                self.trial_info['response_off'] = time.time()
                if self.participant_name == 'Practice' or (self.my_config.getboolean('experiment', 'show_feedback') and
                                                           (self.mode.find('ff') != -1 or
                                                            (not self.my_config.getint('experiment', 'double_click') or
                                                             sel <= self.my_config.getint('experiment',
                                                                                          'confidence_ratings') / 2 or
                                                             self.my_config.getint('experiment', 'double_click') and
                                                             sel > self.my_config.getint('experiment',
                                                                                         'confidence_ratings') / 2 and
                                                             len(self.trial_info['marked_locations']) != 0)) and
                                                           not second_chance):
                    if self.trial_info['signal_present'] and sel > num_ratings / 2 or \
                            not self.trial_info['signal_present'] and sel <= num_ratings / 2:
                        visual.TextStim(win, "CORRECT", colorSpace='rgb255', height=constants.RATING_SIDE / 3,
                                        color=constants.COLOR_WHITE, pos=(0, +constants.RATING_SIDE)).draw()
                    else:
                        visual.TextStim(win, "WRONG", colorSpace='rgb255', height=constants.RATING_SIDE / 3,
                                        color=constants.COLOR_WHITE, pos=(0, +constants.RATING_SIDE)).draw()
                finished = True

            for p in range(0, len(text)):
                text[p].draw()

            win.flip()
            Tools.check_force_quit(self.eyetracker)

            if event.getKeys():
                self.trial_info['response'] = -1

        self.trial_info['response_off'] = time.time()

        time.sleep(.5)

        ask_percentage = self.my_config.get('experiment', 'ask_percentage', fallback=False)
        if ask_percentage:
            self.trial_info['image_covered_answer'] = self.get_response_percentage(win, ask_percentage)

        # core.wait(.5)
        if self.eyetracker and not self.eyetracker.dummy:
            th.start()

        # show feedback!
        if self.participant_name == 'Practice' or \
                (self.trial_info['signal_present'] and
                 (self.mode.find('ff') != -1 or
                  self.my_config.getboolean('experiment', 'show_feedback') and
                  (not self.my_config.getint('experiment', 'double_click') or
                   sel <= self.my_config.getint('experiment', 'confidence_ratings') / 2 or
                   self.my_config.getint('experiment', 'double_click') and
                   sel > self.my_config.getint('experiment', 'confidence_ratings') / 2 and
                   len(self.trial_info['marked_locations']) != 0)
                 ) and not second_chance
                ):

            pos = ((self.pos_display[0] - self.noise_size_display[0] / 2 +
                    self.trial_info['locations'][0] * self.mult_pixel * self.ratio_img_screen[1]),
                   (self.pos_display[1] + self.noise_size_display[1] / 2 -
                    self.trial_info['locations'][1] * self.mult_pixel * self.ratio_img_screen[1]))

            if self.mode.find('dbt') != -1:
                visual.Rect(win, width=2, height=2, fillColor=[0, 0, 0],
                            fillColorSpace="rgb255", units="norm", lineWidth=0).draw()
            else:
                visual.Rect(win, width=2, height=2, fillColor=constants.COLOR_BACKGROUND,
                            fillColorSpace="rgb255", units="norm", lineWidth=0).draw()
            if self.noise_size[2] > 1:
                visual.ImageStim(win, self.noise[self.trial_info['locations'][2] - 1, :, :],
                                 size=self.noise_size_display, pos=self.pos_display, flipVert=True).draw()
                visual.TextStim(win,
                                'Slice (' + str(self.trial_info['locations'][2]) + '/' + str(
                                    self.noise_size[2]) + ')',
                                colorSpace='rgb255', height=constants.RATING_SIDE / 3, units="pix",
                                color=constants.COLOR_WHITE, pos=(0, np.minimum(win.size[1] / 2 - 50,
                                                                                self.noise_size[1] / 2 + 50))).draw()
            else:
                if self.drag_and_drop:
                    self.pos_display = (self.noise_size[0] / 2 - self.trial_info['locations'][0],
                                        -self.noise_size[1] / 2 + self.trial_info['locations'][1])
                    pos = (0, 0)
                visual.ImageStim(win, self.noise, size=self.noise_size_display, pos=self.pos_display).draw()

            visual.TextStim(win, "Press SPACEBAR to continue", colorSpace='rgb255',
                            height=constants.RATING_SIDE / 3, wrapWidth=None,
                            color=constants.COLOR_WHITE,
                            pos=(0, np.maximum(-win.size[1] / 2 + 50, -self.noise_size[1] / 2 - 50))).draw()

            visual.Circle(win, radius=50, lineWidth=4, lineColorSpace="rgb255", units="pix",
                          lineColor=constants.COLOR_WHITE, pos=pos).draw()

            if self.drag_and_drop:  # show scroll bar
                self.draw_minimap(win)

            win.flip()
            event.waitKeys(keyList=['space'])

        # core.wait(.5)
        self.trial_info['response'] = sel

        if self.eyetracker and not self.eyetracker.dummy:
            th.join()

        if second_chance:
            self.second_chance_process_stimuli(th);
            # show same trial again
            visual.TextStim(win, "You got a second chance!", colorSpace='rgb255',
                            height=constants.RATING_SIDE / 3, wrapWidth=None,
                            color=constants.COLOR_WHITE,
                            pos=(0, 0)).draw()
            visual.TextStim(win, "Press SPACEBAR to continue", colorSpace='rgb255',
                            height=constants.RATING_SIDE / 3, wrapWidth=None,
                            color=constants.COLOR_WHITE,
                            pos=(0, np.maximum(-win.size[1] / 2 + 50, -self.noise_size[1] / 2 - 50))).draw()
            win.flip()
            event.waitKeys(keyList=['space'])
            self.second_chance_probability = 0
            self.first_chance = self.trial_info.copy()
            self.trial_info['scrolls'] = []
            self.trial_info['pos'] = []
            self.trial_info['eye_movements'] = []
            self.trial_info['response'] = None
            self.trial_info['stimulus_on'] = -1
            self.trial_info['stimulus_off'] = -1
            self.trial_info['response_on'] = -1
            self.trial_info['response_off'] = -1
            self.trial_info['response'] = -1
            self.trial_info['marked_locations'] = []
            self.show_trial(win)
            self.get_response(win)

    def second_chance_process_stimuli(self, th):
        # self.trial_info['all_saccades'] = all_saccades
        # self.trial_info['scrolls']

        if self.noise_size[2] > 1:
            # self.noise = np.multiply(self.noise, np.logical_not(np.transpose(self.coverage, (2, 0, 1))))
            coverage_mult = np.multiply(np.transpose(self.coverage, (2, 0, 1)).astype('int'), 0.33)
            # coverage_mult = np.multiply(np.transpose(self.coverage, (2, 0, 1)).astype('int'), 0.33) + \
            #     np.logical_not(np.transpose(self.coverage, (2, 0, 1)))
            # self.noise = np.multiply(self.noise, coverage_mult)
            self.noise = np.subtract(self.noise, coverage_mult)
        else:
            # self.noise = np.multiply(self.noise, np.logical_not(self.coverage))
            # coverage_mult = np.multiply(self.coverage.astype('int'), 0.33) + np.logical_not(self.coverage)
            coverage_mult = np.multiply(self.coverage.astype('int'), 0.33)
            self.noise = np.subtract(self.noise, coverage_mult)

        # if self.eyetracker and not self.eyetracker.dummy:
        #     all_saccades = self.trial_info['all_saccades']
        # else:
        #     all_saccades = self.trial_info['eye_movements']
        #
        # all_saccades = [list(all_saccades[i]) for i in range(len(all_saccades))]
        #
        # all_times = all_saccades + [[np.nan, self.trial_info['scrolls'][i][0],
        #    self.trial_info['scrolls'][i][1]] for i in range(len(self.trial_info['scrolls']))]
        #
        # all_times.sort(key=lambda x: x[2])

    def draw_minimap(self, win, scroll_bars=False, minimap=True):
        if scroll_bars:
            visual.Rect(win,
                        width=(self.pos_display[0] / (win.size[0] / 2 - self.noise_size[0] / 2) + 1) * win.size[0] / 2,
                        height=constants.BUTTON_HEIGHT,
                        units="pix", fillColorSpace='rgb255', fillColor=[200, 200, 200], lineWidth=0, opacity=0.7,
                        lineColorSpace="rgb255",
                        pos=[((self.pos_display[0] / (win.size[0] / 2 - self.noise_size[0] / 2) + 1)
                              * win.size[0] / 2) / 2 - win.size[0] / 2,
                             win.size[1] / 2 - constants.BUTTON_HEIGHT / 2]).draw()
            visual.Rect(win, width=constants.BUTTON_HEIGHT,
                        height=(-self.pos_display[1] / (win.size[1] / 2 - self.noise_size[1] / 2) + 1) *
                               win.size[1] / 2,
                        units="pix", fillColorSpace='rgb255', fillColor=[200, 200, 200], lineWidth=0, opacity=0.7,
                        lineColorSpace="rgb255",
                        pos=[-win.size[0] / 2 + constants.BUTTON_HEIGHT / 2,
                             ((self.pos_display[1] / (win.size[1] / 2 - self.noise_size[1] / 2) + 1)
                              * win.size[1] / 2) / 2]).draw()
            # visual.Rect(win, width=win.size[0] * win.size[0] / self.noise_size[0], height=constants.BUTTON_HEIGHT,
            #             units="pix", fillColorSpace='rgb255', fillColor=[200, 200, 200], lineWidth=0, opacity=0.5,
            #             lineColorSpace="rgb255",
            #             pos=[(-self.pos_display[0] + (self.noise_size[0] - win.size[0]) / 2) * win.size[0] /
            #                  self.noise_size[0] - win.size[0] / 2 +
            #                  win.size[0] * win.size[0] / self.noise_size[0] / 2,
            #                  win.size[1] / 2 - constants.BUTTON_HEIGHT / 2]).draw()
            # visual.Rect(win, width=constants.BUTTON_HEIGHT, height=win.size[1] * win.size[1] / self.noise_size[1],
            #             units="pix", fillColorSpace='rgb255', fillColor=[200, 200, 200], lineWidth=0, opacity=0.5,
            #             lineColorSpace="rgb255",
            #             pos=[-win.size[0] / 2 + constants.BUTTON_HEIGHT / 2,
            #                  (-self.pos_display[1] + (self.noise_size[1] - win.size[1]) / 2) * win.size[1] /
            #                  self.noise_size[1] - win.size[1] / 2 +
            #                  win.size[1] * win.size[1] / self.noise_size[1] / 2]).draw()
            # visual.Rect(win, width=win.size[0] * win.size[0] / self.noise_size[0], height=constants.BUTTON_HEIGHT,
            #             units="pix", fillColorSpace='rgb255', fillColor=[200, 200, 200], lineWidth=0, opacity=0.5,
            #             lineColorSpace="rgb255",
            #             pos=[(-self.pos_display[0] + (self.noise_size[0] - win.size[0]) / 2) * win.size[0] /
            #                  self.noise_size[0] - win.size[0] / 2 +
            #                  win.size[0] * win.size[0] / self.noise_size[0] / 2,
            #                  -win.size[1] / 2 + constants.BUTTON_HEIGHT / 2]).draw()
            # visual.Rect(win, width=constants.BUTTON_HEIGHT, height=win.size[1] * win.size[1] / self.noise_size[1],
            #             units="pix", fillColorSpace='rgb255', fillColor=[200, 200, 200], lineWidth=0, opacity=0.5,
            #             lineColorSpace="rgb255",
            #             pos=[win.size[0] / 2 - constants.BUTTON_HEIGHT / 2,
            #                  (-self.pos_display[1] + (self.noise_size[1] - win.size[1]) / 2) * win.size[1] /
            #                  self.noise_size[1] - win.size[1] / 2 +
            #                  win.size[1] * win.size[1] / self.noise_size[1] / 2]).draw()
        if minimap:
            minimap_width = self.noise_size[0] * constants.MINIMAP_HEIGHT / self.noise_size[1]
            visual.Rect(win, width=minimap_width, height=constants.MINIMAP_HEIGHT,
                        units="pix", fillColorSpace='rgb255', fillColor=[200, 200, 200], lineWidth=4, opacity=0.5,
                        lineColorSpace="rgb255", lineColor=[255, 255, 255],
                        pos=[-win.size[0] / 2 + minimap_width / 2 + constants.BUTTON_HEIGHT * 1.5,
                             -win.size[1] / 2 + constants.MINIMAP_HEIGHT / 2 + constants.BUTTON_HEIGHT * 1.5]).draw()

            visual.Rect(win, width=minimap_width * win.size[0] / self.noise_size[0],
                        height=constants.MINIMAP_HEIGHT * win.size[1] / self.noise_size[1],
                        units="pix", fillColorSpace='rgb255', fillColor=[200, 200, 200], lineWidth=2, opacity=0.5,
                        lineColorSpace="rgb255", lineColor=[255, 255, 255],
                        pos=[-win.size[0] / 2 + minimap_width / 2 + constants.BUTTON_HEIGHT * 1.5 - minimap_width * (
                                self.pos_display[0] / self.noise_size[0]),
                             -win.size[
                                 1] / 2 + constants.MINIMAP_HEIGHT / 2 + constants.BUTTON_HEIGHT * 1.5 + constants.MINIMAP_HEIGHT * (
                                     -self.pos_display[1] / self.noise_size[1])]).draw()

    def draw_scrollbar(self, win, current_slice, mouse_pos, mouse_buttons, rescaled_image):

        scroll_bar_height = self.noise_size[1]
        pos_scroll_bar = win.size[0] / 2 - 50

        if rescaled_image:
            scroll_bar_height = win.size[1] * 0.8
            pos_scroll_bar = -win.size[0] / 2 + 100

        step = float(scroll_bar_height) / (self.noise_size[2] - 1)

        visual.Line(win, start=(pos_scroll_bar, scroll_bar_height / 2),
                    end=(pos_scroll_bar, -scroll_bar_height / 2), units="pix", lineWidth=2,
                    lineColorSpace="rgb255", lineColor=constants.COLOR_WHITE).draw()

        if not rescaled_image:
            visual.Line(win, start=(pos_scroll_bar, scroll_bar_height / 2),
                        end=(pos_scroll_bar,
                             scroll_bar_height / 2 - step * (self.trial_info['signal_size'] / 2 - 1)),
                        units="pix", lineWidth=4, lineColorSpace="rgb255", lineColor=[255, 10, 10]).draw()
            visual.Line(win, start=(pos_scroll_bar, -scroll_bar_height / 2),
                        end=(pos_scroll_bar,
                             -scroll_bar_height / 2 + step * (self.trial_info['signal_size'] / 2 - 1)),
                        units="pix", lineWidth=4, lineColorSpace="rgb255", lineColor=[255, 10, 10]).draw()

        scroll_bar = visual.Line(win, start=(pos_scroll_bar - 10, scroll_bar_height / 2 - step * current_slice),
                                 end=(pos_scroll_bar + 10, scroll_bar_height / 2 - step * current_slice), units="pix",
                                 lineWidth=step, lineColorSpace="rgb255", lineColor=constants.COLOR_WHITE)

        scroll_bar.draw()

        next_slice = np.nan
        if any(mouse_buttons) and \
                pos_scroll_bar - 100 < mouse_pos[0] < pos_scroll_bar + 100:
            next_slice = max(0, min(self.noise_size[2] - 1,
                                    self.noise_size[2] - int(
                                        (mouse_pos[1] + step + (scroll_bar_height / 2)) / step)))

        return next_slice - current_slice
