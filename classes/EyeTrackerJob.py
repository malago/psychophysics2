import time
import multiprocessing
from psychopy import core
from classes.Tools import EyeLinkCoreGraphicsPsychopy, Tracker_EyeLink
from collections import namedtuple


class DummyWin:
    def __init__(self, size):
        self.size = size


class EyeTrackerJob(multiprocessing.Process):

    def __init__(self, win_size, dummy):
        # multiprocessing.Process.__init__(self)
        super(EyeTrackerJob, self).__init__()
        dummy_win = DummyWin(win_size)

        self.eyetracker = Tracker_EyeLink(dummy_win, core.Clock(), 1, 1, 'HV9', (128, 128, 128), (0, 0, 0), False,
                                          (dummy_win.size[0], dummy_win.size[1]), dummy)
        self.eye_tracked = 0
        self.shutdown_flag = multiprocessing.Event()
        self.eye_movements = list()
        self.prev_sample = (-1, -1)
        # self.p_output, self.p_input = pipe

    def run(self):
        print('Eye tracker process starts...')

        # self.eyetracker = msg = self.p_output.recv()
        # self.eye_tracked = msg = self.p_output.recv()

        while not self.shutdown_flag.is_set():
            # print('Thread while...')
            # core.wait(1/10) #100 Hz refresh
            time.sleep(1 / 100)
            new_sample = self.eyetracker.getNewestSample(self.eye_tracked)
            if not (new_sample[0] == self.prev_sample[0] and new_sample[1] == self.prev_sample[1]):
                self.prev_sample = (new_sample[0], new_sample[1])
                self.eye_movements.append((new_sample[0], new_sample[1], time.time()))

        print('Eye tracker process dies. We captured ' + str(len(self.eye_movements)) + ' eye movements!')

        # self.p_input.send(self.eye_movements)
