import json
import configparser
from classes.Trial import Trial
from sklearn.utils import shuffle
import numpy as np
import os


class Participant:
    participant_info = {}

    def __init__(self, new_id, my_config, mode, part_info=None):
        if not part_info:
            self.practice = False
            if new_id == 'Practice':
                self.practice = True

            if self.practice:
                max_trials = 10
                signal_absent_ratio = 0
                session_trials = 10
            else:
                max_trials = my_config.getint('experiment', 'max_trials')
                signal_absent_ratio = my_config.getfloat('experiment', 'signal_absent_ratio')
                session_trials = my_config.getint('experiment', 'session_trials')

            # signal_types = ast.literal_eval(my_config.get('experiment', 'signal_sizes'))
            signal_types = list(map(int, my_config.get('experiment', 'signal_sizes').split(',')))
            signal_contrast = list(map(float, my_config.get('experiment', 'signal_contrast').split(',')))

            # trial_order = np.arange(0, max_trials)
            # trial_present = [0] * int(max_trials * signal_absent_ratio) + [1] * int(
            #     max_trials * (1 - signal_absent_ratio))

            trial_present = [1] * int(max_trials * (1 - signal_absent_ratio))
            trial_absent = [0] * int(max_trials * signal_absent_ratio)
            session_present = int(session_trials * (1 - signal_absent_ratio))
            session_absent = int(session_trials * signal_absent_ratio)
            # trial_signal_size = [signal_types[0]] * int(max_trials / 2) + [signal_types[1]] * int(max_trials / 2)
            # trial_signal_size = [signal_types[0], signal_types[1]] * int(max_trials / 2)

            p_order, p_present, p_absent = 0, 0, 0
            trial_order = []
            trial_presence = []
            trial_signal_size = []
            for s in range(int(np.ceil(max_trials / session_trials))):
                new_trial_order = list(range(p_order + 0, p_order + session_trials))
                new_trial_presence = trial_present[p_present:p_present+session_present]
                new_trial_presence += trial_absent[p_absent:p_absent+session_absent]
                new_trial_signal_size = [signal_types[0], signal_types[1]] * int(session_trials / 2)

                new_trial_presence, new_trial_order, new_trial_signal_size = shuffle(new_trial_presence,
                                                                                     new_trial_order,
                                                                                     new_trial_signal_size)
                trial_order += list(new_trial_order)
                trial_presence += list(new_trial_presence)
                trial_signal_size += list(new_trial_signal_size)

                p_order += session_trials
                p_present += session_present
                p_absent += session_absent

            # trial_present, trial_order, trial_signal_size = shuffle(trial_present, trial_order, trial_signal_size)
            # random.shuffle(trial_order)
            # random.shuffle(trial_signal_size)

            trial_present = trial_presence

            if self.practice:
                max_trials = 99999
                trial_present = [1] * max_trials
                signal_absent_ratio = 0
                # signal_contrast *= 1.5

            self.mode = mode
            # read images and shuffle
            if self.mode.find('dbt') != -1:
                # load phantom
                phantom_directories = my_config.get('config', 'phantom_directory').split(',')
                images_list = []
                # list files
                for i in range(0, len(phantom_directories)):
                    phantom_list = [f for f in os.listdir(phantom_directories[i]) if
                                    os.path.isdir(os.path.join(phantom_directories[i], f)) and f.find('del') == -1]

                    phantom_present = [f for f in phantom_list if
                                       os.path.exists(os.path.join(phantom_directories[i], f, 'locations.txt'))]
                    phantom_absent = [f for f in phantom_list if
                                      not os.path.exists(os.path.join(phantom_directories[i], f, 'locations.txt'))]

                    if self.practice:
                        phantom_absent = []
                        phantom_present = phantom_present[:10]

                    ph_list = [os.path.join(phantom_directories[i], f) for f in phantom_present[0:int(
                        (1 - signal_absent_ratio) * (max_trials / len(phantom_directories)))]]
                    images_list.extend(ph_list)
                    ph_list = [os.path.join(phantom_directories[i], f) for f in phantom_absent[
                                                                                0:int(signal_absent_ratio * (
                                                                                        max_trials / len(
                                                                                    phantom_directories)))]]

                    images_list.extend(ph_list)
            else:
                # get noise fields
                images_list = []

            trial_directory = my_config.get('config', 'trial_directory').split(',')

            self.participant_info = {'pid': new_id, 'num_trials': 0,
                                     'modes': my_config.get('experiment', 'modes').split(','),
                                     'max_trials': max_trials,
                                     'results_dir': my_config.get('experiment', 'results_dir'),
                                     'trial_order': trial_order,
                                     'session_trials': my_config.get('experiment', 'session_trials'),
                                     'signal_absent_ratio': signal_absent_ratio,
                                     'signal_contrast': signal_contrast,
                                     'trial_present': trial_present,
                                     'trial_signal_size': trial_signal_size,
                                     'images_list': images_list,
                                     'trial_directory': my_config.get('config', 'trial_directory'),
                                     'signals_file': my_config.get('config', 'signals_file')}

            self.save_participant()
        else:
            self.practice = False
            self.participant_info = part_info
            self.mode = mode

    def to_json(self):
        ret = json.dumps(self.participant_info, default=lambda o: o.__dict__,
                         sort_keys=True, indent=4)
        return ret

    def save_participant(self):
        if not self.practice:

            if not os.path.exists(
                    self.participant_info['results_dir'] + "/" + self.participant_info['pid'] + "/" + self.mode):
                os.mkdir(self.participant_info['results_dir'] + "/" + self.participant_info['pid'] + "/" + self.mode)

            participant_file = open(
                self.participant_info['results_dir'] + "/" + self.participant_info['pid'] +
                "/" + self.mode + "/" + "ExpData.viu", 'w')
            participant_file.write(self.to_json())
            participant_file.close()

    @staticmethod
    def from_json(my_config, participant_string, mode):
        json_str = my_config.get('experiment', 'results_dir') + "/" + participant_string + "/" + mode + "/ExpData.viu"
        with open(json_str) as json_file:
            json_dict = json.load(json_file)

        # json_data = recordtype("ParticipantInfo", json_dict.keys())(*json_dict.values())

        # return Participant(participant_string, my_config,
        #                    ParticipantInfo(participant_string, json_dict['num_trials'], json_dict['max_trials'],
        #                                    my_config.get('experiment', 'results_dir')))

        return Participant(participant_string, my_config, mode, json_dict)
