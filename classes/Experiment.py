from psychopy import visual, core, event
from classes import constants
from classes.Participant import Participant
from classes.Trial import Trial
from classes.Tools import ThreadWithReturnValue
from classes.Tools import Tools
from classes.Tools import Tracker_EyeLink
import glob
import configparser
import os
import scipy
from scipy import io
from scipy import ndimage
import hdf5storage
import numpy as np
import time
import matplotlib
import gzip
from pyedfread import edf

# import mkl_fft

matplotlib.use('Agg')


class Experiment:
    # my_mouse: Mouse
    # my_config: configparser
    # win: visual.Window

    def __init__(self, win, selected_option=0):
        self.win = win
        self.my_mouse = event.Mouse()
        # with open("ExpOptions.ini") as f:
        #     t_config = f.read()
        self.my_config = configparser.RawConfigParser(allow_no_value=True)
        self.my_config.read("ExpOptions.ini")
        self.eyetracker = None
        self.th = None
        self.power_spectrum = None
        self.distractor = []
        self.modes = self.my_config.get('experiment', 'modes').split(',')
        self.trial_directory = self.my_config.get('config', 'trial_directory').split(',')
        self.signals_file = self.my_config.get('config', 'signals_file').split(',')
        self.results_dir = self.my_config.get('experiment', 'results_dir')
        self.current_version = '??????'
        self.eye_tracked = self.my_config.getboolean('experiment', 'eye_tracked')

        self.signals = [None] * len(self.modes)

        if self.my_config.getboolean('experiment', 'force_mode_choice', fallback=0):
            self.selected_option = -np.Inf
        else:
            self.selected_option = selected_option

        self.current_mode = 0

    def calibrate_tracker(self, eye_tracker_enabled):
        # settings = Settings()
        # disp = libscreen.Display(disptype='psychopy')
        # self.eyetracker = eyetracker.EyeTracker(disp, 'eyelink')
        # # self.eyetracker.start_recording()
        # self.eyetracker.calibrate()

        dummy = False
        if eye_tracker_enabled == 'dummy':
            dummy = True

        self.eyetracker = Tracker_EyeLink(self.win, core.Clock(), 1, 1, 'HV9', (128, 128, 128), (0, 0, 0), False,
                                          (self.win.size[0], self.win.size[1]), dummy)

        # self.eyetracker.startEyeTracking(1, False, self.win.size[0], self.win.size[1])

        # while True:
        #     tracker_event = self.eyetracker.getNextData()
        #     if tracker_event == 5:
        #         print(tracker_event)
        #     keys = Tools.check_force_quit()
        #     if keys:
        #         if 'return' in keys:
        #             break

        # self.eyetracker.startEyeTracking(-1, calibTrial=True, widthPix=self.win.size[0], heightPix=self.win.size[1])
        # self.eyetracker.stopEyeTracking()
        # self.eyetracker.retrieveDataFile()

    def get_participants(self):
        logo_image = visual.ImageStim(self.win, constants.LOGOURL, units="pix", pos=(0, self.win.size[1] / 2 - 150))
        logo_image.autoDraw = True
        participants = [f for f in glob.glob(self.my_config.get('experiment', 'results_dir') + "/*")]
        # progress = [len(glob.glob(f)) for f in os.listdir(participants)]
        participants_len = len(participants)
        participants = [p.split('\\')[-1] for p in participants]

        versions = [f for f in os.listdir(r'.git/refs/tags') if os.path.isfile(os.path.join(r'.git/refs/tags', f))]
        vv = []
        for v in versions:
            v_split = str.split(v, '.')
            vv.append((int(v_split[0]), int(v_split[1]), int(v_split[2])))

        vv.sort(reverse=True)

        self.current_version = '.'.join(str(c) for c in vv[0])

        options = [f for f in os.listdir(r'.') if f.startswith('ExpOptions')]

        participants.append("New observer")
        # participants.reverse()

        buttons = []
        for i in range(0, len(participants)):
            buttons.append((0, self.win.size[1] / 2 - logo_image.size[1] - 100 - (
                    constants.BUTTON_HEIGHT + 10) * i + constants.BUTTON_HEIGHT / 2 + 7, constants.BUTTON_WIDTH,
                            constants.BUTTON_HEIGHT))

        participants.append("Practice")
        buttons.append((-self.win.size[0] / 2 + 50 + constants.BUTTON_WIDTH / 4,
                        -self.win.size[1] / 2 + 50 + constants.BUTTON_HEIGHT / 2,
                        constants.BUTTON_WIDTH / 4, constants.BUTTON_HEIGHT))

        participants.append("X")
        buttons.append((self.win.size[0] / 2 - 200 + 100,
                        self.win.size[1] / 2 - 200 + 100,
                        50, 50))

        prev_options = len(participants)

        options_title = []
        for i in range(0, len(options)):
            self.my_config.read(options[i])
            buttons.append((50 - self.win.size[0] / 2 + constants.BUTTON_WIDTH / 4, self.win.size[1] / 2 - 100 - (
                    constants.BUTTON_HEIGHT + 10) * i + constants.BUTTON_HEIGHT / 2 + 7, constants.BUTTON_WIDTH / 2,
                            constants.BUTTON_HEIGHT))
            participants.append(self.my_config.get("experiment", "title"))
            options_title.append(self.my_config.get("experiment", "title"))

        self.my_config.clear()
        self.my_config.read(options[max(0, self.selected_option)])
        self.modes = self.my_config.get('experiment', 'modes').split(',')
        self.results_dir = self.my_config.get('experiment', 'results_dir')

        progress = self.get_participant_progress(participants[:participants_len])

        rect = []
        text = []
        for p in range(0, len(buttons)):
            rect.append(visual.Rect(self.win, width=buttons[p][2], height=buttons[p][3], units="pix",
                                    fillColorSpace='rgb255',
                                    fillColor=[0.7 * x for x in constants.COLOR_RED], lineWidth=0, pos=buttons[p][0:2],
                                    autoDraw=True))
            text.append(visual.TextStim(self.win, participants[p], color=constants.COLOR_WHITE, units="pix",
                                        colorSpace="rgb255", pos=buttons[p][0:2]))
            text[p].autoDraw = True
            if participants[p] in options_title:
                text[p].height /= 1.5

        visual.Rect(self.win, width=constants.BUTTON_WIDTH / 1.5, height=constants.BUTTON_HEIGHT + 75, units="pix",
                    fillColorSpace='rgb255', fillColor=[0.7 * x for x in constants.COLOR_RED], lineWidth=0,
                    pos=(+self.win.size[0] / 2 - constants.BUTTON_WIDTH / 1.5,
                         -self.win.size[1] / 2 + 165), autoDraw=True)
        visual.Rect(self.win, width=constants.BUTTON_WIDTH / 1.5 - 8, height=constants.BUTTON_HEIGHT + 36, units="pix",
                    fillColorSpace='rgb255', fillColor=constants.COLOR_GRAY, lineWidth=0,
                    pos=(+self.win.size[0] / 2 - constants.BUTTON_WIDTH / 1.5,
                         -self.win.size[1] / 2 + 165 - constants.BUTTON_HEIGHT / 2), autoDraw=True)
        contact_text = visual.TextStim(self.win, 'Contact', alignText='center',
                                       pos=(+self.win.size[0] / 2 - constants.BUTTON_WIDTH / 1.5,
                                            -self.win.size[1] / 2 + 200))
        contact_text.autoDraw = True
        contact_text2 = visual.TextStim(self.win, 'lago@psych.ucsb.edu', alignText='center',
                                        pos=(+self.win.size[0] / 2 - constants.BUTTON_WIDTH / 1.5,
                                             -self.win.size[1] / 2 + 165))
        contact_text2.autoDraw = True
        contact_text3 = visual.TextStim(self.win, '805-506-5606', alignText='center',
                                        pos=(+self.win.size[0] / 2 - constants.BUTTON_WIDTH / 1.5,
                                             -self.win.size[1] / 2 + 135))
        contact_text3.autoDraw = True

        selected = False
        ret = None
        while not selected or self.selected_option < 0:
            visual.TextStim(self.win, 'v' + self.current_version, height=15, alignText='left',
                            pos=(-self.win.size[0] / 2 + 30, self.win.size[1] / 2 - 30)).draw()
            # logo_image.draw()
            # print(mouse_position)
            # visual.Circle(self.win, radius=5, lineWidth=1, lineColorSpace="rgb255", units="pix",
            #               lineColor=constants.COLOR_WHITE, pos=self.my_mouse.getPos()).draw()
            for p in range(0, len(participants)):
                # text[p].text = text[p].text
                if rect[p].contains(self.my_mouse.getPos()) or p - prev_options == self.selected_option:
                    rect[p].fillColor = constants.COLOR_RED
                    if p < len(progress) and self.selected_option >= 0:
                        visual.TextStim(self.win, ("%.1f" % (progress[p] * 100)) + "%", height=15, alignText='left',
                                        pos=[buttons[p][0] + constants.BUTTON_WIDTH / 2 + 10, buttons[p][1]]).draw()
                else:
                    rect[p].fillColor = [0.7 * x for x in constants.COLOR_RED]
                if p < prev_options and self.selected_option < 0 and participants[p] != "X":
                    rect[p].fillColor = constants.COLOR_DISABLED

                if self.my_mouse.isPressedIn(rect[p]):
                    if participants[p] in options_title:
                        self.selected_option = p - prev_options
                        self.my_config.clear()
                        self.my_config.read(options[self.selected_option])
                        self.modes = self.my_config.get('experiment', 'modes').split(',')
                        self.trial_directory = self.my_config.get('config', 'trial_directory').split(',')
                        self.signals_file = self.my_config.get('config', 'signals_file').split(',')
                        self.results_dir = self.my_config.get('experiment', 'results_dir')
                        self.signals = [None] * len(self.modes)
                        progress = self.get_participant_progress(participants[:participants_len])
                    elif participants[p] == "X":
                        ret = None
                        self.selected_option = 0
                        selected = True
                    elif participants[p] == 'New observer' and self.selected_option >= 0:
                        for p2 in range(0, len(participants)):
                            text[p2].autoDraw = False
                            rect[p2].autoDraw = False
                        ret = self.load_participant(self.new_participant())

                        for p2 in range(0, len(participants)):
                            text[p2].autoDraw = True
                            rect[p2].autoDraw = True
                        selected = True
                    elif participants[p] == 'Practice' and self.selected_option >= 0:
                        ret = self.load_participant('Practice')
                        selected = True
                    elif self.selected_option >= 0:
                        ret = self.load_participant(participants[p])
                        selected = True

            self.win.flip()
            Tools.check_force_quit()

        self.win._toDraw = []

        self.win.flip()

        return ret

    def new_participant(self):

        ftext = [visual.TextStim(self.win, "First name:", color=constants.COLOR_WHITE, units="pix",
                                 colorSpace="rgb255", pos=(-150, 60), alignText='left', anchorHoriz='left'),
                 visual.TextStim(self.win, "Last name:", color=constants.COLOR_WHITE, units="pix",
                                 colorSpace="rgb255", pos=(-150, 30), alignText='left', anchorHoriz='left'),
                 visual.TextStim(self.win, "Age:", color=constants.COLOR_WHITE, units="pix",
                                 colorSpace="rgb255", pos=(-150, 0), alignText='left', anchorHoriz='left'),
                 visual.TextStim(self.win, "Gender:", color=constants.COLOR_WHITE, units="pix",
                                 colorSpace="rgb255", pos=(-150, -30), alignText='left', anchorHoriz='left')]

        lines = [visual.Line(self.win, start=(-20, 45), end=(250, 45), units="pix", lineWidth=2,
                             lineColorSpace="rgb255", lineColor=constants.COLOR_WHITE),
                 visual.Line(self.win, start=(-20, 15), end=(250, 15), units="pix", lineWidth=2,
                             lineColorSpace="rgb255", lineColor=constants.COLOR_WHITE),
                 visual.Line(self.win, start=(-20, -15), end=(250, -15), units="pix", lineWidth=2,
                             lineColorSpace="rgb255", lineColor=constants.COLOR_WHITE),
                 visual.Line(self.win, start=(-20, -45), end=(250, -45), units="pix", lineWidth=2,
                             lineColorSpace="rgb255", lineColor=constants.COLOR_WHITE)]

        for i in range(0, len(ftext)):
            ftext[i].autoDraw = True
            # lines[i].autoDraw = True

        new_participant = ['', '', '', '']

        section = 0
        selected = False
        while not selected:
            visual.Rect(self.win, width=2, height=2, fillColor=constants.COLOR_GRAY,
                        fillColorSpace="rgb255", units="norm", lineWidth=0).draw()
            lines[section].draw()

            for i in range(0, len(ftext)):
                visual.TextStim(self.win, new_participant[i], color=constants.COLOR_WHITE, units="pix",
                                colorSpace="rgb255", pos=(-20, 45 - 30 * i), alignText='left',
                                anchorVert='bottom', anchorHoriz='left').draw()

            self.win.flip()

            keys = Tools.check_force_quit()
            if keys:
                if 'return' in keys or 'tab' in keys:
                    section += 1
                elif 'backspace' in keys:
                    new_participant[section] = new_participant[section][:-1]
                elif 'space' in keys:
                    new_participant[section] += ' '
                elif len(keys[0]) == 1:
                    new_participant[section] += keys[0].upper()

            if section == 4:
                selected = True
                try:
                    age = int(new_participant[2])
                    if age < 18 or len(new_participant[0]) < 3 or len(new_participant[1]) < 3:
                        selected = False
                        new_participant = ['', '', '', '']
                        section = 0
                except ValueError:
                    selected = False
                    new_participant = ['', '', '', '']
                    section = 0

        new_participant[0] = new_participant[0][0:3]
        new_participant[1] = new_participant[1][0:3]
        new_participant[3] = new_participant[3][0]

        for i in range(0, len(ftext)):
            ftext[i].autoDraw = False

        ret = new_participant[0] + "_" + new_participant[1] + "_" + new_participant[2] + new_participant[3]

        try:
            os.mkdir(self.results_dir + "/" + ret)
            # for m in self.modes:
            #     os.mkdir(self.results_dir + "/" + ret + "/" + m)
        except:
            print("Can't create folder...")

        return ret

    def get_participant_progress(self, participants):
        progress = [0] * (len(participants))
        max_trials = self.my_config.getint('experiment', 'max_trials')
        for p in range(len(participants)):
            if os.path.exists(os.path.join(self.results_dir, participants[p], self.modes[0])):
                files = os.listdir(os.path.join(self.results_dir, participants[p], self.modes[0]))
                progress[p] = ((len(files) - 1) / max_trials)
        return progress

    def load_participant(self, participant_string):

        # visual.TextStim(self.win, "Loading participant " + participant_string + "...").draw()
        # self.win.flip()

        results_dir = self.results_dir + "/" + participant_string + "/" + self.modes[self.current_mode]

        trials = [f for f in glob.glob(results_dir + "/*")]

        if len(trials) == 0:
            self.win._toDraw = []
            visual.TextStim(self.win, "Loading " + participant_string + ", please wait... ",
                            colorSpace='rgb255', height=constants.RATING_SIDE / 3, wrapWidth=None,
                            color=constants.COLOR_WHITE, pos=(0, 0)).draw()
            self.win.flip()
            return Participant(participant_string, self.my_config, self.modes[self.current_mode])
        else:
            return Participant.from_json(self.my_config, participant_string, self.modes[self.current_mode])

    def generate_trial(self, trial_id, signal_present, signal_size, signal_contrast, mode, images_list):
        locations_dist = []
        if self.modes[mode].find('numbers') != -1:
            noise = np.zeros([820, 1024, 1])
            locations = [511, 409, 0]
            signal_present = True
            signal_contrast = 1
        elif self.modes[mode].find('dbt') != -1:
            # load phantom
            # noise = hdf5storage.loadmat(os.path.join(images_list[trial_id], 'phantom.mat'))
            # noise = h5py.File(os.path.join(images_list[trial_id], 'phantom.mat'), 'r')

            if os.path.exists(os.path.join(images_list[trial_id], 'phantom.npz')):
                noise = np.load(os.path.join(images_list[trial_id], 'phantom.npz'))
                noise = noise['noise']
            else:
                noise = scipy.io.loadmat(os.path.join(images_list[trial_id], 'phantom.mat'))
                noise = noise['tiff']

                window_level = 10987
                window_width = 11841

                # noise[noise > window_level + window_width / 2] = window_level + window_width / 2
                # noise[noise < window_level - window_width / 2] = window_level - window_width / 2

                window_max = np.ones(noise.shape, dtype=np.uint16) * np.uint16(window_level + window_width / 2)
                window_min = np.ones(noise.shape, dtype=np.uint16) * np.uint16(window_level - window_width / 2)

                # with gpu(0):
                np.minimum(noise, window_max, out=noise)
                np.maximum(window_min, noise, out=noise)
                # noise = )

                noise_min = window_level - window_width / 2
                noise_max = window_level + window_width / 2

                # noise = np.multiply(np.subtract(np.divide(np.subtract(noise, noise_min), noise_max - noise_min), 0.5), 2)
                noise = (((noise - noise_min) / (noise_max - noise_min)) - 0.5) * 2

            # np.savez_compressed(os.path.join(images_list[trial_id], 'phantom.npz'), noise=noise)

            locations_file = os.path.join(images_list[trial_id], 'locations.txt')
            if os.path.exists(locations_file):
                read_locations = open(locations_file).read().split()
                locations = (int(float(read_locations[0])),
                             int(float(read_locations[2])),
                             int(float(read_locations[1])) + 1)
                signal_present = 1
                signal_size = float(read_locations[3])
                signal_contrast = float(read_locations[4])
            else:
                locations = (-1, -1, np.random.randint(10, noise.shape[2] - 10))
                signal_present = 0
                signal_contrast = 0
        else:
            # load noise field
            if not self.signals[self.current_mode]:
                self.signals[self.current_mode] = hdf5storage.loadmat(self.signals_file[self.current_mode])

            file_name = r'' + self.trial_directory[self.current_mode] + '\\noise' + str(trial_id + 1) + '.mat'
            if not os.path.isfile(file_name):
                if not self.power_spectrum:
                    self.power_spectrum = scipy.io.loadmat(self.my_config.get('config', 'power_spectrum_file'))
                noise = np.random.randn(self.power_spectrum['S'].shape[0], self.power_spectrum['S'].shape[1],
                                        self.power_spectrum['S'].shape[2])
                noise_fft = np.multiply(np.fft.fftn(noise),
                                        np.sqrt(np.multiply(self.power_spectrum['S'], np.power(90, 2))))
                # noise_fft = np.multiply(mkl_fft.fftn(noise), np.sqrt(np.multiply(s['S'], np.power(90, 2))))
                noise = np.swapaxes(np.maximum(0, np.minimum(255, np.add(128, np.real(np.fft.ifftn(noise_fft))))), 1, 3)
                # noise = np.maximum(0, np.minimum(255, np.add(128, np.real(mkl_fft.ifftn(noise_fft)))))
                # save noise field as matlab matrix
            else:
                noise = scipy.io.loadmat(file_name)
                noise = noise['noise']

            locations = (-1, -1, 0)

            if signal_size < 10:
                s = 0
            else:
                s = 1

            signal_orig = self.signals[self.current_mode]['signal'][0][s] * signal_contrast[self.current_mode]

            if len(noise.shape) == 3:
                max_slices = self.my_config.getint('experiment', 'max_slices', fallback=noise.shape[2])

                if max_slices != noise.shape[2]:
                    noise = noise.take(range(0, max_slices), axis=2)
                    signal_orig = signal_orig.take(range(int(signal_orig.shape[2] / 2 - max_slices / 2),
                                                         int(signal_orig.shape[2] / 2 + max_slices / 2)), axis=2)

            # signal_orig = self.signals['signal'][0][0]
            # self.distractor.append(scipy.ndimage.zoom(signal_orig, (1, 1, 2), mode='nearest', prefilter=False))
            # signal_orig = self.signals['signal'][0][1]
            # self.distractor.append(scipy.ndimage.zoom(signal_orig, (1, 1, 2), mode='nearest', prefilter=False))
            # np.savez_compressed('distractors.npz', noise=self.distractor)

            if signal_present:
                np.random.seed(trial_id)
                if len(noise.shape) == 3:
                    locations = (
                        np.random.randint(signal_size * 2, noise.shape[1] - signal_size * 4 + 1),
                        np.random.randint(signal_size * 2, noise.shape[0] - signal_size * 4 + 1),
                        np.random.randint(signal_size / 2, noise.shape[2] - signal_size / 2 + 1))
                    # locations = (300, 100, 0)
                    signal_shifted = np.roll(signal_orig, int(-noise.shape[0] / 2 + locations[1]), axis=0)
                    signal_shifted = np.roll(signal_shifted, int(+noise.shape[1] / 2 + locations[0]), axis=1)
                    signal_shifted = np.roll(signal_shifted, int(-noise.shape[2] / 2 + locations[2]), axis=2)
                else:
                    locations = (
                        np.random.randint(signal_size * 2, noise.shape[1] - signal_size * 4),
                        np.random.randint(signal_size * 2, noise.shape[0] - signal_size * 4),
                        1)
                    # locations = (300, 100, 0)
                    signal_shifted = np.roll(signal_orig, int(-noise.shape[0] / 2 + locations[1]), axis=0)
                    signal_shifted = np.roll(signal_shifted, int(+noise.shape[1] / 2 + locations[0]), axis=1)
                noise = np.minimum(noise + signal_shifted, 255)
            else:
                time.sleep(1)

            n_distractors = 0
            if self.my_config.getint('experiment', 'distractors') > 0:
                n_distractors = np.random.randint(0, self.my_config.getint('experiment', 'distractors'))
                if not self.distractor:
                    # self.distractor.append(np.uint16(1e4 * scipy.ndimage.zoom(self.signals['signal'][0][0], (1, 1, 4),
                    #                                                           mode='nearest', prefilter=False)))
                    # self.distractor[-1] = self.distractor[-1][:, :,
                    #                       int(self.distractor[-1].shape[2] / 2 - noise.shape[2] / 2):int(
                    #                           self.distractor[-1].shape[2] / 2 + noise.shape[2] / 2)]
                    # self.distractor.append(np.uint16(1e4 * scipy.ndimage.zoom(self.signals['signal'][0][1], (1, 1, 4),
                    #                                                           mode='nearest', prefilter=False)))
                    # self.distractor[-1] = self.distractor[-1][:, :,
                    #                       int(self.distractor[-1].shape[2] / 2 - noise.shape[2] / 2):int(
                    #                           self.distractor[-1].shape[2] / 2 + noise.shape[2] / 2)]
                    # np.save('distractors', self.distractor)
                    self.distractor = np.load(self.my_config.get('config', 'distractors_file'))

            for dist in range(n_distractors):
                locations_dist.append([
                    np.random.randint(signal_size, noise.shape[1] - signal_size),
                    np.random.randint(signal_size, noise.shape[0] - signal_size),
                    np.random.randint(signal_size / 2, noise.shape[2] - signal_size / 2)])
                if self.modes[mode].find('2D') != -1:
                    locations_dist[-1][2] = locations[2]
                distractor_n = np.roll(np.float16(self.distractor[s]), int(-noise.shape[0] / 2 + locations_dist[-1][1]),
                                       axis=0)
                distractor_n = np.roll(distractor_n, int(+noise.shape[1] / 2 + locations_dist[-1][0]), axis=1)
                distractor_n = np.roll(distractor_n, int(-noise.shape[2] / 2 + locations_dist[-1][2]), axis=2)
                noise = np.minimum(noise + 1e-4 * signal_contrast[s] * distractor_n, 255)

            noise = np.multiply(np.subtract(np.divide(np.array(noise, dtype=float), 255), 0.5), 2)

        if self.modes[mode].find('2D') != -1:
            if len(noise.shape) == 3:
                noise = np.flipud(noise[:, :, locations[2]])
            else:
                noise = np.flipud(noise)
        if len(noise.shape) == 3:
            # noise = np.swapaxes(np.swapaxes(noise, 0, 2), 1, 2)
            noise = np.transpose(noise, (2, 0, 1)).copy()
        # stimulus = [None] * noise.shape[2]
        # for s in range(0, noise.shape[2]):
        #     stimulus[s] = visual.ImageStim(self.win, noise[:, :, s], size=noise.shape[0:2])
        # print(noise.shape)
        return {'noise': noise, 'locations': locations, 'signal_present': signal_present,
                'signal': self.signals[self.current_mode], 'signal_size': signal_size,
                'signal_contrast': signal_contrast, 'distractors': locations_dist}

    def load_stimuli(self, trial_id, signal_present, signal_size, signal_contrast, mode, images_list):
        # self.generate_trial(trial_id, signal_present, signal_size, signal_contrast, mode, images_list)
        th = ThreadWithReturnValue(target=self.generate_trial,
                                   args=(trial_id, signal_present, signal_size, signal_contrast, mode, images_list))
        th.start()
        return th

    def show_next_trial(self, participant_str):

        participant = self.load_participant(participant_str)

        if not self.th:
            while participant.participant_info['num_trials'] >= self.my_config.getint('experiment', 'max_trials'):
                self.current_mode += 1
                if self.current_mode >= len(participant.participant_info["modes"]):
                    return False
                participant = self.load_participant(participant_str)

        if not self.th:
            self.th = self.load_stimuli(
                participant.participant_info['trial_order'][participant.participant_info['num_trials']],
                participant.participant_info['trial_present'][participant.participant_info['num_trials']],
                participant.participant_info['trial_signal_size'][participant.participant_info['num_trials']],
                participant.participant_info['signal_contrast'], self.current_mode,
                participant.participant_info['images_list'])

        count = 0
        while self.th.is_alive():
            dots = ['.'] * (count % 20)
            if self.modes[self.current_mode].find('dbt') != -1:
                visual.Rect(self.win, width=2, height=2, fillColor=[0, 0, 0],
                            fillColorSpace="rgb255", units="norm", lineWidth=0).draw()
            else:
                visual.Rect(self.win, width=2, height=2, fillColor=constants.COLOR_BACKGROUND,
                            fillColorSpace="rgb255", units="norm", lineWidth=0).draw()

            visual.TextStim(self.win,
                            "Generating trial " + str(participant.participant_info['num_trials'] + 1),
                            colorSpace='rgb255', height=constants.RATING_SIDE / 3, wrapWidth=None,
                            color=constants.COLOR_WHITE, pos=(0, 0)).draw()
            visual.TextStim(self.win, ''.join(dots),
                            colorSpace='rgb255', height=constants.RATING_SIDE / 3, wrapWidth=None,
                            color=constants.COLOR_WHITE, pos=(0, -50)).draw()
            self.win.flip()
            core.wait(.1)
            count += 1

        prev_mode = self.current_mode
        self.current_mode = (self.current_mode + 1) % len(self.modes)

        t = Trial(participant.participant_info['pid'],
                  participant.participant_info['trial_order'][participant.participant_info['num_trials']],
                  participant.participant_info['num_trials'] + 1, self.modes[prev_mode],
                  self.my_config, self.th.join(), self.eyetracker, self.current_version)

        # load next trial while response is being computed
        prev_participant = participant
        prev_participant.participant_info['num_trials'] += 1

        participant = self.load_participant(participant.participant_info['pid'])
        try:
            self.th = self.load_stimuli(
                participant.participant_info['trial_order'][participant.participant_info['num_trials']],
                participant.participant_info['trial_present'][participant.participant_info['num_trials']],
                participant.participant_info['trial_signal_size'][participant.participant_info['num_trials']],
                participant.participant_info['signal_contrast'], self.current_mode,
                participant.participant_info['images_list'])
        except:
            self.th = None

        responded = False
        while not responded:
            invalid_text = ""
            # print(self.signals[self.current_mode])
            if t.show_trial(self.win):
                t.get_response(self.win)
            if t.trial_info['response'] == -1:
                invalid_text = 'Invalid response'
                responded = True
            elif self.modes[prev_mode].find('ff') == -1 and (self.my_config.getint('experiment', 'double_click') and
                                                             (t.trial_info['response'] > self.my_config.getint(
                                                                 'experiment', 'confidence_ratings') / 2 and
                                                              len(t.trial_info['marked_locations']) == 0)):
                invalid_text = 'Please mark one location'
            else:
                responded = True
            if not responded:
                visual.TextStim(self.win, invalid_text).draw()
                self.win.flip()
                core.wait(2)

        prev_participant.save_participant()

        if not prev_participant.participant_info['pid'] == 'Practice':
            if self.my_config.getboolean('experiment', 'zip_results', fallback=False):
                with gzip.GzipFile(
                        self.results_dir + "/" + prev_participant.participant_info['pid'] + "/" +
                        self.modes[prev_mode] + "/data" + str(prev_participant.participant_info['num_trials']) +
                        ".viz", 'w') as fid_gz:
                    fid_gz.write(bytes(str(t.trial_info), 'utf8'))
            else:
                t1, t2 = t.to_json()
                trial_file = open(
                    self.results_dir + "/" + prev_participant.participant_info['pid'] + "/" + self.modes[
                        prev_mode] +
                    "/data" + str(prev_participant.participant_info['num_trials']) + ".viu", 'w')
                trial_file.write(t2)
                trial_file.close()
                if t1:
                    trial_file = open(
                        self.results_dir + "/" + prev_participant.participant_info['pid'] + "/" + self.modes[
                            prev_mode] +
                        "/data" + str(prev_participant.participant_info['num_trials']) + "-first.viu", 'w')
                    trial_file.write(t1)
                    trial_file.close()

        return self.th is not None

    def show_session_end(self, cont):
        visual.Rect(self.win, width=2, height=2, fillColor=constants.COLOR_GRAY,
                    fillColorSpace="rgb255", units="pix", lineWidth=0).draw()
        if cont:
            visual.TextStim(self.win,
                            'You completed the trials for this session, please come back next day!',
                            color=constants.COLOR_WHITE, units="pix", pos=(0, 0), height=constants.RATING_SIDE / 3,
                            colorSpace="rgb255", alignText='center').draw()
        else:
            visual.TextStim(self.win,
                            'You completed all the trials, thanks for participating!',
                            color=constants.COLOR_WHITE, units="pix", pos=(0, 0), height=constants.RATING_SIDE / 3,
                            colorSpace="rgb255", alignText='center').draw()

        self.win.flip()
        core.wait(2)
