import os
import scipy
from scipy import io
import numpy as np
from psychopy import visual, core, event, monitors
# from PIL import Image
from classes.Experiment import Experiment
import time
from classes import constants
import sys
from win32api import GetSystemMetrics
from classes import Trial
import os

# phantom_directories = []
# images_list = []
# phantom_directories.append("E:\Phantoms\Jan18\calcs")
# phantom_directories.append("E:\Phantoms\Jan18\masses")
# for i in range(1,len(phantom_directories)):
#     phantom_list = [f for f in os.listdir(phantom_directories[i]) if
#                     os.path.isdir(os.path.join(phantom_directories[i], f))]
#
#     phantom_present = [f for f in phantom_list if
#                        os.path.exists(os.path.join(phantom_directories[i], f, 'locations.txt'))]
#     phantom_absent = [f for f in phantom_list if
#                       not os.path.exists(os.path.join(phantom_directories[i], f, 'locations.txt'))]
#
#     images_list.extend(phantom_present)
#     images_list.extend(phantom_absent)
#
#     for j in range(len(images_list)):
#         print(str(j) + " of " + str(len(images_list)))
#         noise = scipy.io.loadmat(os.path.join(phantom_directories[i], images_list[j], 'phantom.mat'))
#         noise = noise['tiff']
#
#         window_level = 10987
#         window_width = 11841
#
#         # noise[noise > window_level + window_width / 2] = window_level + window_width / 2
#         # noise[noise < window_level - window_width / 2] = window_level - window_width / 2
#
#         window_max = np.ones(noise.shape, dtype=np.uint16) * np.uint16(window_level + window_width / 2)
#         window_min = np.ones(noise.shape, dtype=np.uint16) * np.uint16(window_level - window_width / 2)
#
#         # with gpu(0):
#         np.minimum(noise, window_max, out=noise)
#         np.maximum(window_min, noise, out=noise)
#         # noise = )
#
#         noise_min = window_level - window_width / 2
#         noise_max = window_level + window_width / 2
#
#         # noise = np.multiply(np.subtract(np.divide(np.subtract(noise, noise_min), noise_max - noise_min), 0.5), 2)
#         noise = (((noise - noise_min) / (noise_max - noise_min)) - 0.5) * 2
#         np.savez_compressed(os.path.join(phantom_directories[i], images_list[j], 'phantom.npz'), noise=noise)
# exit(-1)

# class Solution:
#     def generateParenthesis(self, n: int):
#         self.gen_recursive(n, n, "", [])
#
#     def convert(self, s: str, numRows: int) -> str:
#         new_str = ""
#
#         current_col = 0
#         current_row = 0
#         c = 0
#         down = 1
#
#         rows = [''] * numRows
#
#         while c < len(s):
#             rows[current_row] += s[c]
#
#             if current_row == numRows - 1:
#                 down = 0
#
#             if down:
#                 current_row += 1
#             elif current_row == 1:
#                 down = 1
#                 current_row=0
#             else:
#                 current_row -= 1
#
#             c += 1
#         print(rows)
#
#
# S = Solution()
# S.convert("PAYPALISHIRING",3)
# exit()

monitor = monitors.Monitor('testMonitor')
monitor.setDistance(75)

if sys.platform == 'win32':
    monitor_size = (GetSystemMetrics(0), GetSystemMetrics(1))
else:
    monitor_size = (800, 600)

win = visual.Window(fullscr=True, size=monitor_size, colorSpace='rgb255',
                    color=constants.COLOR_GRAY, units="pix", monitor=monitor)

my_experiment = Experiment(win)

# participant: Participant
exit_experiment = False
while not exit_experiment:
    try:
        participant = my_experiment.get_participants()
        session_start = time.time()
        if participant:
            current_session_trials = 0

            eye_tracker_enabled = my_experiment.my_config.get('experiment', 'eye_tracker')

            if not eye_tracker_enabled == 'none':
                my_experiment.calibrate_tracker(eye_tracker_enabled)

            cont = True
            while cont and current_session_trials < my_experiment.my_config.getint('experiment', 'session_trials') and \
                    time.time() - session_start < my_experiment.my_config.getfloat('experiment',
                                                                                   'session_time',
                                                                                   fallback=np.inf) * 60:
                cont = my_experiment.show_next_trial(participant.participant_info['pid'])
                current_session_trials += 1

            my_experiment.show_session_end(cont)
        else:
            exit_experiment = True
    except SystemExit:
        win._toDraw = []
        my_experiment = Experiment(win, selected_option=my_experiment.selected_option)
    except:
        # win.close()
        # win = visual.Window(fullscr=True, colorSpace='rgb255', color=constants.COLOR_GRAY, units="pix")
        # print(e)
        print(sys.exc_info())
        raise

win.close()
